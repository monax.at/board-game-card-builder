### v0.1.2 (11.03.2020)
- Added new handler 'skip' for ignore fields in csv file  
 
### v0.1.1 (26.02.2020) 
- Added new handler for displaying list of groups
- Added new command `process` for `build` and `convert` groups by one command
- Added default value for group name, it's name of template svg file   

### v0.1.0 (24.02.2020)
- Added two commands: build and convert
- Updated structure of config file, added `rasterization` option
- Added integration with inkscape for rasterization of cards 

### v0.0.6 (19.02.2020)

- Added logic for processing multilines text from csv files.   
  Lines should to be separated by `\n` symbol.

### v0.0.5 (18.02.2020)

- Renamed 'collection' to 'group'.
- Added options for console command: --group,-g and --item, -i.

### v0.0.4 (04.02.2020)

- Added basic functionality
