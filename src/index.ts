#!/usr/bin/env node

import commander from "commander";
import {TemplateToSvgHandler} from "./handler/template-to-svg-handler";
import {SvgToBitmapHandler} from "./handler/svg-to-bitmap-handler";
import {ViewListHandler} from "./handler/view-list-handler";

const program = new commander.Command();
program.version("0.1.2");

program.command("build")
  .description("create cards based on json, svg-template and csv description")
  .option("-c, --config [file]", "config file path", "config.json")
  .option("-g, --group <groupName>", "named block from config.json")
  .option("-i, --item <cardName>", "name of card from csv, required block option")
  .action(cmd => new TemplateToSvgHandler().run(cmd));

program.command("convert")
  .description("convert svg files to png")
  .option("-c, --config [file]", "config file path", "config.json")
  .option("-g, --group <groupName>", "named block from config.json")
  .option("-i, --item <cardName>", "name of card from csv, required block option")
  .action(cmd => new SvgToBitmapHandler().run(cmd));

program.command("process")
  .description("run both 'build' and 'convert' commands")
  .option("-c, --config [file]", "config file path", "config.json")
  .option("-g, --group <groupName>", "named block from config.json")
  .option("-i, --item <cardName>", "name of card from csv, required block option")
  .action(cmd => {
    new TemplateToSvgHandler().run(cmd).then(() => new SvgToBitmapHandler().run(cmd));
  });

program.command("list")
  .description("view list of existed groups")
  .option("-c, --config [file]", "config file path", "config.json")
  .action(cmd => new ViewListHandler().run(cmd));

if (process.argv.length === 2) {
  process.argv.push("-h");
}

program.parse(process.argv);
