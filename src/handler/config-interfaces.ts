export interface GroupConfig {
  name?: string;
  workDir?: string;
  outputDir?: string;
  templateFile: string;
  csvConfigFile: string;
  rasterization?: RasterizationConfig;
}

export interface RasterizationConfig {
  enable?: boolean;
  dpp?: number;
}

export interface AppConfig {
  groups: GroupConfig[];
}

export interface CardConfig {
  key: string;
  data: any[];
}
