import {BaseHandler} from "./base-handler";
import {CardConfig, GroupConfig} from "./config-interfaces";
import commandExists from "command-exists";
import colors from "colors";
import fs from "fs";
import path from "path";
import pParse from "papaparse";
import { exec } from "child_process";

export class SvgToBitmapHandler extends BaseHandler {
  protected readonly name = "SvgToBitmap";
  protected readonly toolName = "inkscape";

  async run(cmd: {config: string; group?: string; item?: string}) {
    try {
      if (!await commandExists(this.toolName)) {
        return console.log(colors.red("can't find installed inkscape or inkscape not accessible in terminal"));
      }
      super.run(cmd);
    } catch (e) {
      return console.log(colors.red(e));
    }
  }

  protected processGroup(group: GroupConfig, cardName?: string): void {
    console.log(colors.green.bold(`${this.name}: Rasterization of group template: ${group.templateFile}`));

    const csvConfigFile = fs.readFileSync(path.resolve(process.cwd(), group.workDir, group.csvConfigFile));

    if (group.rasterization === false || group.rasterization?.enable === false) {
      console.log(colors.green(`${this.name}: Skip group template: ${group.templateFile}`));
      return;
    }

    pParse.parse(csvConfigFile.toString(), {complete: result => {
        const config = this.parseCSVConfig(result.data);
        if (!cardName) {
          config.forEach(card => this.processCard(card, group));
        } else {
          const card = config.find(item => item.key === cardName);
          if (!card) {
            return console.log(colors.red(`${this.name}: Can't find card with name: ${cardName}`));
          }
          this.processCard(card, group);
        }
      }});
  }

  protected processCard(card: CardConfig, groupConfig: GroupConfig) {
    const fileName = path.resolve(process.cwd(), groupConfig.outputDir, card.key);
    const dpp = groupConfig.rasterization?.dpp || 300;

    exec(`${this.toolName} -z -e=${fileName}.png ${fileName}.svg -d=${dpp}`,
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
    });
  }

}
