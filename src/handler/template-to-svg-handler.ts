import {BaseHandler} from "./base-handler";
import {CardConfig, GroupConfig} from "./config-interfaces";
import colors from "colors";
import fs from "fs";
import path from "path";
import pParse from "papaparse";
import xmldom from "xmldom";
import {createLinker} from "./linkers/create-linker";

/**
* process templates and create svg files, that can be edited in inkscape
*/
export class TemplateToSvgHandler extends BaseHandler {
  protected readonly name = "TemplateToSvg";

  protected processGroup(group: GroupConfig, cardName?: string): void {
    console.log(colors.green.bold(`${this.name}: Process group template: ${group.templateFile}`));

    const templateFile = fs.readFileSync(path.resolve(process.cwd(), group.workDir, group.templateFile));
    const csvConfigFile = fs.readFileSync(path.resolve(process.cwd(), group.workDir, group.csvConfigFile));

    pParse.parse(csvConfigFile.toString(), {complete: result => {
      const config = this.parseCSVConfig(result.data);
      if (!cardName) {
        config.forEach(card => this.processCard(card, group, templateFile));
      } else {
        const card = config.find(item => item.key === cardName);
        if (!card) {
          return console.log(colors.red(`${this.name}: Can't find card with name: ${cardName}`));
        }
        this.processCard(card, group, templateFile);
      }
    }});
  }

  protected processCard(card: CardConfig, groupConfig: GroupConfig, template: Buffer) {
    const DOMParser = xmldom.DOMParser;
    const XMLSerializer = xmldom.XMLSerializer;
    const doc = (new DOMParser()).parseFromString(template.toString());

    card.data.forEach(configItem => {
      const [key, type, value, options] = configItem;
      const linker = createLinker(type, groupConfig);
      if (linker) {
        linker.run(doc, key, value, options);
      }
    });

    if (!fs.existsSync(groupConfig.outputDir)) {
      fs.mkdirSync(groupConfig.outputDir,  {recursive: true});
    }

    fs.writeFile(path.resolve(process.cwd(), groupConfig.outputDir, `${card.key}.svg`),
      (new XMLSerializer()).serializeToString(doc),
      (err) => {
        if (err) throw err;
        console.log(card.key + " - Saved!");
      }
    );
  }

}
