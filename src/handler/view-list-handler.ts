import {BaseHandler} from "./base-handler";
import {GroupConfig} from "./config-interfaces";
import colors from "colors";

export class ViewListHandler extends BaseHandler {
  name = "ViewListHandler";

  protected processGroup(group: GroupConfig): void {
    console.log(colors.bold(`${group.name}`));
  }
}
