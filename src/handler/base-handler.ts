import {AppConfig, CardConfig, GroupConfig} from "./config-interfaces";
import fs from "fs";
import colors from "colors";

export abstract class BaseHandler {
  protected readonly name: string;

  protected abstract processGroup(group: GroupConfig, cardName?: string): void;

  async run(cmd: {config: string; group?: string; item?: string}) {
    try {
      fs.readFile(cmd.config, (err, data) => {
        if (err) {
          throw err;
        }

        const appConfig: AppConfig = JSON.parse(data.toString());
        if (!appConfig.groups?.length) {
          return console.log(colors.red(`${this.name}: App config, groups is empty`));
        }

        /** setting default names for groups */
        appConfig.groups.forEach(group => group.name = group.name || group.templateFile);
        if (!cmd.group) {
          appConfig.groups.forEach(group => this.processGroup(group));
        } else {
          const group = appConfig.groups.find(item => item.name === cmd.group);
          if (!group) {
            return console.log(colors.red(`${this.name}: Can't find collection with name: ${cmd.group}`));
          }
          this.processGroup(group, cmd.item);
        }
      });
    } catch(err) {
      return console.error(colors.red(err));
    }
  }

  protected parseCSVConfig(data: any[], skipTitle = true): CardConfig[] {
    if (skipTitle) {
      data.shift();
    }

    const result: CardConfig[] = [];
    let currentCollection: CardConfig = null;
    let key = "";
    data.forEach(rowRaw => {
      const row = rowRaw.map((item: string) => item.trim());

      if (row[0].length !== 0 && key !== row[0]) {
        currentCollection = {key: row[0], data: []};
        result.push(currentCollection);
        key = row[0];
      }
      /** add only when row contain fields with keys and values */
      if (row.length > 1) {
        /** removing first item, key field not needed */
        row.shift();
        currentCollection.data.push(row);
      }
    });

    return result;
  }

}
