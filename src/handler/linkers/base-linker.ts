/**  */
export interface BaseLinker {
  run(doc: Document, key: string, value: string, options?: string): void;
}
