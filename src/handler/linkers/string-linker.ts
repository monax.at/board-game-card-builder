import {BaseLinker} from "./base-linker";

export class StringLinker implements BaseLinker {
  run(doc: Document, key: string, value: string): void {
    const text = doc.getElementById(key);
    if (!text) {
      throw new Error("StringLinker: can't find node with id: " + key);
    }

    /** 'text' type is a common one line text node, usable for titles / short descriptions */
    if (text.tagName === "text") {
      text.getElementsByTagName("tspan")[0].textContent = value;
    }

    /** this is embed to box multiline text in inkscape,
     * flowRoot and flowPara is tags from svg1.2 specification, modern browsers not support this tags but they uses in inkscape
     */
    if (text.tagName === "flowRoot") {
      const baseTextNode = text.getElementsByTagName("flowPara")[0];
      const templateTextNode = baseTextNode.cloneNode();
      text.removeChild(baseTextNode);

      value.split("\n").forEach(textLine => {
        const textNode = templateTextNode.cloneNode();
        textNode.textContent = textLine;
        text.appendChild(textNode);
      });
    }
  }
}
