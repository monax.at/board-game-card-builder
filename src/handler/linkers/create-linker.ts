import {BaseLinker} from "./base-linker";
import {SvgLinker} from "./svg-linker";
import {StringLinker} from "./string-linker";
import {ImageLinker} from "./image-linker";
import {RemoverLinker} from "./remover-linker";
import {GroupConfig} from "../config-interfaces";

export function createLinker(type: string, collectionConfig: GroupConfig): BaseLinker | null {
  switch (type) {
    case "string": return new StringLinker();
    case "svg": return new SvgLinker(collectionConfig);
    case "image": return new ImageLinker(collectionConfig);
    case "delete": return new RemoverLinker();
    case "skip": return null;
  }
  throw new Error("CreateLinker: Can't recognize type of svg linker: " + type);
}
