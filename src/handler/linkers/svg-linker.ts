import {BaseLinker} from "./base-linker";
import xmldom from "xmldom";
import fs from "fs";
import {GroupConfig} from "../config-interfaces";
import path from "path";

export class SvgLinker implements BaseLinker {
  constructor(public collectionConfig: GroupConfig) { }

  run(doc: Document, key: string, value: string): void {
    const node = doc.getElementById(key);
    if (!node) {
      throw new Error("SvgLinker: can't find node with id: " + key);
    }
    if (node.tagName !== "rect") {
      throw new Error("SvgLinker: node must be a rect for replacing, id: " + key);
    }

    /** file names */
    const svgFileNames = value.split(",").map(val => val.trim());
    const DOMParser = xmldom.DOMParser;

    const [x, y, width, height] = [
      node.getAttribute("x"),
      node.getAttribute("y"),
      node.getAttribute("width"),
      node.getAttribute("height")
    ];

    svgFileNames.forEach((svgFileName: string) => {
      const templateFile = fs.readFileSync(path.resolve(process.cwd(), this.collectionConfig.workDir, svgFileName));
      const template = (new DOMParser()).parseFromString(templateFile.toString());
      const templateSvg = template.getElementsByTagName("svg")[0];
      templateSvg.setAttribute("x", x);
      templateSvg.setAttribute("y", y);
      templateSvg.setAttribute("width", width);
      templateSvg.setAttribute("height", height);

      doc.insertBefore(templateSvg, node);
    });

    doc.removeChild(node);
  }
}
