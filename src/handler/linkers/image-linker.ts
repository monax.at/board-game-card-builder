import {BaseLinker} from "./base-linker";
import {GroupConfig} from "../config-interfaces";
import fs from "fs";
import path from "path";

export class ImageLinker implements BaseLinker {
  constructor(public collectionConfig: GroupConfig) { }

  run(doc: Document, key: string, value: string): void {
    const node = doc.getElementById(key);
    if (!node) {
      throw new Error("ImageLinker: can't find node with id: " + key);
    }
    if (node.tagName !== "image") {
      throw new Error("ImageLinker: node must be a image for replacing, id: " + key);
    }


    const templateFile = fs.readFileSync(path.resolve(process.cwd(), this.collectionConfig.workDir, value));
    const templateData = templateFile.toString("base64");
    node.setAttribute("xlink:href", "data:image/png;base64," + templateData);
  }
}
