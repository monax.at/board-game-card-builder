import {BaseLinker} from "./base-linker";
import colors from "colors";

export class RemoverLinker implements BaseLinker {
  run(doc: Document, key: string): void {
    const node = doc.getElementById(key);
    if (!node) {
      return console.log(colors.yellow("RemoverLinker: Warning! can't find node with id: " + key));
    }
    doc.removeChild(node);
  }
}
