## cli tool for building cards for tabletop games

This is a CLI-tool for bulk creating cards for tabletop game prototyping.  
Tool ruled by json and svg configuration files for building cards 
based on one svg-template and several resources that described in csv file.  
Besides the [Inkscape](https://inkscape.org) is used for rasterization of svg cards, it should be installed in your OS and accessible in terminal

Ready to use example you can download by this link [google disk](https://drive.google.com/open?id=1IEUTvPQXE7WwGOxKZfxQQ62mQBijs2Mq).  
Link to repository: [gitlab](https://gitlab.com/monax.at/board-game-card-builder).
  
### Requirements
node.js 10+ 

### Installation  
Installation as global package:   
`npm install -g svg-card-creator`

### Configuration and using

1. Create config.json file.
2. Create svg template file.  
    For template creation I use [Inkscape](https://inkscape.org)  
    Blocks for replacing must contain named ID.
    
3. Create csv table with key-based description of each card  

4. Type and run in console: `bcardc` or `bcardc -h` or `bcardc --help` to view help.  

Available commands:
    * **list** - shows list of the groups, described in config.json    
    * **build** - create svg files of each card, described in json and csv files.
    * **convert** - convert cards from svg to png.   
    Required installed inkscape and executed of 'build' command.
    * **process** - run both 'build' and 'convert' commands 

Each command have options:  
- -g, --group - if you wish to rebuild one group, you need to set `name` for this group and run  
`bcardc build -g <name>` or `bcardc convert -g <name>`
- -i, --item - used for rebuilding of one card in group, you need run command with card name option:
`bcardc build -g <groupName> -i <cardName>` or `bcardc convert -g <groupName> -i <cardName>`
    
#### config.json syntax

Example:  
```json 
{
  "groups": [
    {
      "name": "group name (optional param)",
      "workDir": "data",
      "outputDir": "data/result/images",
      "templateFile": "border - image.svg",
      "csvConfigFile":"cards - images.csv",
      "rasterization": {
        "enable": true,
        "dpp": 300
      }
    }
  ]
}
```

**groups** - array of configurations, usable for separation a cards by type   
**name** - _optional_, used for handling group by name    
**workDir** - _optional_, directory where placed templates and configuration files  
**outputDir** - _optional_, directory where tool will save result of work
**templateFile** - svg template file used as base for cards    
**csvConfigFile** - csv file that contain instructions for tool how to process template and create cards
**rasterization** - _optional_, object with two properties:
- **enable** - optional, boolean value, by default 'true', can be used for disabling rasterization in some purposes 
- **dpp** - dot per pixels, by default value is 300  

#### Svg template requirements

Tool can process several types of shapes and objects in svg:
1. rect - used for replacing by svg templates. 
    Tool will read rect size and position and will use for inserting data from another svg files. 
    Can be assigned several files into one rect block
2. text and flowRoot & flowPara - used for inserting text.  
    contend of `text` tag will be simple replaced. 
    For `flowRoot & flowPara` tags situation little bit more complicated:   
    Inkscape use flowRoot & flowPara for multiline text inbounded by rect `(Text > Flow into Frame)`.
    But browsers not support this feature and will not display any text if you try to use this feature, 
    but in graphics editor all will be displayed fine       
3. image - content of tag will be replaced by image, size and position will be leave without changes

#### csv config file syntax

Example:

|card|key                          |type  |value                                        |
|----|-----------------------------|------|---------------------------------------------|
|example|title                     |string|Hi example                                   |
|    |text                         |string|Lorem ipsum dolor sit amet, an eum vero mollis sensibus, id usu modus nihil ubique.  At sumo altera pertinacia pro, pri vocibus prodesset et. Eu cum eripuit fabellas. Aliquip expetendis assueverit vel ad. Sea id adhuc essent utamur. |
|    |image                        |image |test-image.png                               |
|    |icon                         |svg   |test-icon-1.svg, test-icon-2.svg             |
|    |non-used                     |delete|item for deleting from template              |
|    |for-ignore                   |skip  |data for ignore                              |

Columns:
1. card - name that will be used for processed card file
2. key - id of element in template file 
3. type - type of action that will be applied to element of template 
    * string - text value
    * image - png file name that will be read and used in template
    * svg - one or more svg file names, separated by comma
    * delete - remove part of template by key (Id)
    * skip - ignore key and data in csv file

4. value - data for linker that will be used for generate new card (text or file names)

### Roadmap


 
* Grouping cards to printable atlases
* Text binging without using flowRoot tags (browsers support)
* GUI for tool (software or web)    
